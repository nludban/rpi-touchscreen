#!/usr/bin/env python3

from tkinter import *

import enum
import pprint

Sun = enum.Enum('Sun', 'YELLOW RED BLUE')
Diet = enum.Enum('Diet', 'OMNIVORE VEGANS PALEO')
Atmosphere = enum.Enum('Atmosphere', dict(
    PHLOGISTICATED='Phlogisticated',
    DEPHLOGISTICATED='Dephlogisticated',
    PURE_PHLOGISTON='99% Phlostiston'))

#---------------------------------------------------------------------#

class MainWindow(Tk):

    def __init__(self, **kwargs):
        Tk.__init__(self, **kwargs)
        #self.tk_bisque()

        menu = self._build_menu(self)
        self.configure(menu=menu)

        self._text = Text(self, width=50, height=10, state=DISABLED)
        self._text.grid(sticky=NSEW)

        return

    def _build_menu(self, master):
        menu = Menu(master)

        filemenu = Menu(menu)
        filemenu.add_command(label='New')
        filemenu.add_command(label='Open...')
        filemenu.add_separator()
        filemenu.add_command(label='Quit', command=self.on_quit)
        menu.add_cascade(label='File', menu=filemenu)

        menu.add_command(label='Register', command=self.on_register)

        helpmenu = Menu(menu)
        helpmenu.add_command(label='Help')
        helpmenu.add_command(label='About')
        menu.add_cascade(label='Help', menu=helpmenu)

        return menu

    def on_quit(self):
        self.quit()

    def on_register(self):
        #print('!')
        dlg = RegistrationDialog(self)
        result = dlg.run_modal(self)
        s = pprint.pformat(result, indent=3)
        self._text.configure(state=NORMAL)
        self._text.delete('1.0', END)
        self._text.insert(END, s)
        self._text.configure(state=DISABLED)
        return

#---------------------------------------------------------------------#

#// http://effbot.org/tkinterbook/tkinter-dialog-windows.htm
class RegistrationDialog(Toplevel):

    def __init__(self, master, **kwargs):
        Toplevel.__init__(self, master, **kwargs)
        self.master = master
        self.title('Alien Registration Form')

        f = self._create_form(self)
        f.grid(sticky=NSEW, padx=20, pady=10)

        f = self._create_buttons(self)
        f.grid(sticky=EW, padx=20, pady=10)

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        return

    def _create_form(self, master):
        f = Frame(master)#, background='blue')

        self._sun_color = StringVar(self, value=Sun.YELLOW)
        self._atmosphere = StringVar(self,
                                     value=Atmosphere.DEPHLOGISTICATED)
        self._diet = StringVar(self, value=Diet.OMNIVORE)

        #k = Frame(f, relief=GROOVE, border=3)
        #k.grid(row=1, column=1, columnspan=2, sticky=NSEW)
        Label(f, text='Sun Color:').grid(row=1, column=1, sticky=NE)
        g = Frame(f)
        Radiobutton(g, value=Sun.YELLOW, text='Yellow',
                    variable=self._sun_color).grid(sticky=W)
        Radiobutton(g, value=Sun.RED, text='Red',
                    variable=self._sun_color).grid(sticky=W)
        Radiobutton(g, value=Sun.BLUE, text='Blue',
                    variable=self._sun_color).grid(sticky=W)
        g.grid(row=1, column=2, sticky=W)

        Label(f, text='Atmosphere:').grid(row=2, column=1, sticky=NE)
        g = Frame(f)
        Radiobutton(g, value=Atmosphere.PHLOGISTICATED,
                    text='Phlogisticated',
                    variable=self._atmosphere).grid(sticky=W)
        Radiobutton(g, value=Atmosphere.DEPHLOGISTICATED,
                    text='Dephlogisticated',
                    variable=self._atmosphere).grid(sticky=W)
        Radiobutton(g, value=Atmosphere.PURE_PHLOGISTON,
                    text='99% Phlogiston',
                    variable=self._atmosphere).grid(sticky=W)
        g.grid(row=2, column=2)

        Label(f, text='Diet:').grid(row=3, column=1, sticky=NE)
        g = Frame(f)
        Radiobutton(g, value=Diet.OMNIVORE, text='Omnivore',
                    variable=self._diet).grid(sticky=W)
        Radiobutton(g, value=Diet.VEGANS, text='Vegans',
                    variable=self._diet).grid(sticky=W)
        Radiobutton(g, value=Diet.PALEO, text='Paleo',
                    variable=self._diet).grid(sticky=W)
        g.grid(row=3, column=2, sticky=W)

        #sun color - red yellow ?

        #atmosphere - phlogisticated dephlogisticated 99% phlogiston

        #diet - carnivore vegan low carb paleo

        return f

    def validate(self):
        self._result = dict(
            sun_color	= self._sun_color.get(),
            atmosphere	= self._atmosphere.get(),
            diet	= self._diet.get(),
        )
        return True

    def _create_buttons(self, master):
        f = Frame(master, background='green')

        Button(f, text='Register', width=10,
               command=self.on_register
        ).grid(row=1, column=1)

        Button(f, text='Cancel', width=10,
               command=self.on_cancel
        ).grid(row=1, column=2)

        self.bind('<Escape>', self.on_cancel)

        return f

    def run_modal(self, master):
        self._result = None
        self.transient(master)
        self.grab_set()
        self.protocol('WM_DELETE_WINDOW', self.on_cancel)
        #self.geometry('+%i+%i' % ( ))
        #self.?.focus_set()
        self.wait_window(self)
        return self._result

    def on_register(self):
        if not self.validate():
            return
        self.master.focus_set()
        self.destroy()
        return

    def on_cancel(self, *args):
        self.master.focus_set()
        self.destroy()
        return

#---------------------------------------------------------------------#

def main():
    mw = MainWindow()
    mw.mainloop()

main()

#--#
