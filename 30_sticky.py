#!/usr/bin/env python3

from tkinter import *

#// The application is a Frame which goes into a Toplevel (Tk)
#// supplied by main().  This makes the app reusable as a component
#// frame within some future application.
def main():
    top = Tk()
    top.title('Example 42')	# Not working in ChromeOS?
    app = DemoAppFrame(top)
    app.pack(expand=YES, fill=BOTH)
    app.mainloop()

#---------------------------------------------------------------------#

class DemoAppFrame(Frame):

    def __init__(self, master, **kwargs):
        Frame.__init__(self, master, **kwargs)

        f = self._variables = VariableFrame(self)
        f.configure(border=5, relief=GROOVE)
        f.grid(sticky=NSEW)

        f = self._buttons = ButtonsFrame(self, background='blue')
        f.grid(sticky=EW, padx=20, pady=20, ipadx=10, ipady=10)

        self.rowconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

        return

    def reset(self):
        self._variables.reset()


class VariableFrame(Frame):

    def __init__(self, master, **kwargs):
        Frame.__init__(self, master, **kwargs)

        sv = self._number = StringVar(value='dos')
        sv.trace_add('write', self.on_change)

        self._create_widgets(self)
        return

    def reset(self):
        self._number.set('dos')

    def _create_widgets(self, master):
        sv = self._number

        self._label = Label(master, textvariable=sv,
                            background='#ffffe0',
                            font='Times 16 underline')
        self._label.grid(row=3, column=1, columnspan=3, pady=5)

        RBSTYLE = dict(width=8, anchor=W,
                       #//https://en.wikipedia.org/wiki/X11_color_names
                       #// or .../lib/X11/rgb.txt
                       background='salmon',
                       foreground='sea green',	# XXX Text and indicator.
                       font=('Times', 17))

        Radiobutton(master, variable=sv, text='One', value='uno', **RBSTYLE
        ).grid(row=1, column=1)

        Radiobutton(master, variable=sv, text='Two', value='dos', **RBSTYLE
        ).grid(row=1, column=2)

        Radiobutton(master, variable=sv, text='Three', value='tres', **RBSTYLE
        ).grid(row=1, column=3)

        Entry(master, textvariable=sv, font='Fixed 24 bold italic overstrike'
        ).grid(row=2, column=1, columnspan=3)

        om = OptionMenu(
            master, sv,
            'one', 'two', 'three',
            'uno', 'dos', 'tres')
        om.configure(font=('Courier', 18))
        om.grid(row=4, column=1, columnspan=2)

        return

    TAB = dict(
        uno	= W,
        dos	= EW,
        tres	= E,
    )

    def on_change(self, *args):
        #print(args)
        value = self._number.get()
        self._label.grid(sticky=self.TAB.get(value, ''))
        return


class ButtonsFrame(Frame):

    def __init__(self, parent, **kwargs):
        Frame.__init__(self, parent, **kwargs)
        self._parent = parent

        Button(self, text='Reset', command=self.on_reset).grid(
            row=1, column=1, sticky=E, padx=10, pady=10)

        Button(self, text='Quit', command=self.on_quit).grid(
            row=1, column=2, sticky=E, ipadx=10, ipady=10)

        return

    def on_reset(self): return self._parent.reset()

    def on_quit(self): return self._parent.quit()

#---------------------------------------------------------------------#

main()

#--#
