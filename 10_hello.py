#!/usr/bin/env python3
#
# 10_hello.py -- Hello world.
#

#// -or- import tkinter as tk
from tkinter import *

# Connect to desktop screen server and create
# the first window.
#// Also known as the "root" window.
top = Tk()

# Create a label in the window.
lbl = Label(
    master	= top,
    text	= 'Hello, world!',
    font	= ('Courier', 42),
)

# Request the pack geometry manager to set the
# position and size.
lbl.pack_configure()

# Run the GUI...
top.mainloop()

#--#
