#!/usr/bin/env python3
#
# 11_hello.py -- Dehydrated hello world.
#

from tkinter import *

top = Tk()

import tkinter.font as font
print('families=', sorted(font.families()))

#f = font.Font(family='Monospace', size=42)
f = font.Font(family='Nimbus Mono L', size=42)
print(f.actual())
Label(top, text='Hello, world!', font=f).pack()

top.mainloop()

#--#
