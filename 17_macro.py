#!/usr/bin/env python3

from tkinter import *

class ApplicationWindow(Frame):

    def __init__(self, master, **kwargs):
        Frame.__init__(self, master, **kwargs)
        self._master = master

        body = self._create_body(master,
                                 relief=GROOVE, border=10,
                                 background='red')
        body.pack(expand=YES, fill=BOTH)

        sbar = self._create_statusbar(master,
                                      relief=GROOVE, border=10,
                                      background='blue')
        sbar.pack(side=BOTTOM, fill=X)

        return

    def on_quit(self):
        # Confirm quit w/o saving etc?
        return self.quit()

    def build_menu(self, master):
        menu = Menu(master)

        filemenu = Menu(menu)
        filemenu.add_command(label='New')
        filemenu.add_command(label='Open...')
        filemenu.add_separator()
        filemenu.add_command(label='Quit',
                             command=self.on_quit)
        menu.add_cascade(label='File', menu=filemenu)

        helpmenu = Menu(menu)
        helpmenu.add_command(label='Help')
        helpmenu.add_command(label='About')
        menu.add_cascade(label='Help', menu=helpmenu)

        return menu

    def _create_body(self, master, **kwargs):
        f = Frame(master, **kwargs)

        self._create_body_right(f, relief=GROOVE, bd=5).pack(
            side=RIGHT, fill=Y)

        self._create_body_middle(f, relief=GROOVE, bd=5).pack(
            side=RIGHT)

        self._create_body_left(f, relief=GROOVE, bd=5).pack(
            side=RIGHT, anchor=N)

        return f

    def _create_body_left(self, master, **kwargs):
        f = Frame(master, **kwargs)
        Label(f, text='Button 1', relief=GROOVE, border=2,
              height=2, width=10).pack()
        Label(f, text='Two', relief=GROOVE, border=2,
              height=2).pack(fill=X)
        Label(f, text='C', relief=GROOVE, border=2,
              height=2).pack(fill=X)
        return f

    def _create_body_middle(self, master, **kwargs):
        f = Frame(master, **kwargs)
        c = Canvas(f, background='green', width=200, height=200)
        c.pack(side=TOP)
        c.create_oval(20, 20, 180, 180, fill='yellow')

        c.create_line(45, 80, 65, 80)
        c.create_line(135, 80, 155, 80)

        c.create_line(40, 130, 160, 130)

        Scrollbar(f, orient=HORIZONTAL).pack(side=BOTTOM, fill=X)
        return f

    def _create_body_right(self, master, **kwargs):
        f = Frame(master, **kwargs)
        Scrollbar(f, orient=VERTICAL).pack(expand=YES, fill=Y)
        return f

    def _create_statusbar(self, master, **kwargs):
        f = Frame(master, **kwargs)
        Label(f, text='Status:', relief=RAISED).pack(side=LEFT)
        Label(f, text='meh.', relief=SUNKEN).pack(side=LEFT)
        return f

top = Tk()
top.configure(background='yellow')

app = ApplicationWindow(top)
app.pack(expand=YES)

mbar = app.build_menu(top)
top.configure(menu=mbar)

top.mainloop()

#--#
