#!/usr/bin/env python3
#
# 20_tabs.py - Various failed attempts to evenly space items using pack.
#

from tkinter import *

top = Tk()
top.configure(background='red')

df = Frame(top, background='green')
df.pack(side=TOP, fill=BOTH, expand=YES)

t1 = Frame(df, background='gray')
Label(t1, text='#1').pack()
t1.pack()

t2 = Frame(df, background='orange')
Label(t2, text='#2').pack()

t3 = Frame(df, background='yellow')
Label(t3, text='#3').pack()

def show_tab(t):
    t1.pack_forget()
    t2.pack_forget()
    t3.pack_forget()
    t.pack(fill=BOTH)

bf = Frame(top, background='blue')
bf.pack(side=BOTTOM, fill=X, padx=5, pady=5)

if False:
    # 1,2,3....
    Button(bf, text='Door #1',
           command=lambda : show_tab(t1)).pack(side=LEFT)
    Button(bf, text='Door #2',
           command=lambda : show_tab(t2)).pack(side=LEFT)
    Button(bf, text='Door #3',
           command=lambda : show_tab(t3)).pack(side=LEFT)

if False:
    # 1, 2/3
    Button(bf, text='Door #1',
           command=lambda : show_tab(t1)).pack(side=LEFT)
    Button(bf, text='Door #2',
           command=lambda : show_tab(t2)).pack()
    Button(bf, text='Door #3',
           command=lambda : show_tab(t3)).pack(side=RIGHT)

#Button(bf, text='Door #2', command=lambda : show_tab(t2)).pack(side=CENTER)
#_tkinter.TclError: bad side "center": must be top, bottom, left, or right

if True:
    # 1/2/3
    Button(bf, text='Door #1',
           command=lambda : show_tab(t1)).pack(anchor=W)
    Button(bf, text='Door #2',
           command=lambda : show_tab(t2)).pack(anchor=CENTER)
    Button(bf, text='Door #3',
           command=lambda : show_tab(t3)).pack(anchor=E)

top.mainloop()

#--#
