#!/usr/bin/env python3

from tkinter import *

class ScrolledCanvas(Frame):

    def __init__(self, master, **kwargs):
        Frame.__init__(self, master, **kwargs)

        self._canvas = Canvas(self, width=720, height=480)
        self._canvas.grid(row=1, column=1, sticky=NSEW)

        hs = Scrollbar(self, orient=HORIZONTAL,
                       command=self._canvas.xview)
        hs.grid(row=2, column=1, sticky=EW)

        vs = Scrollbar(self, orient=VERTICAL,
                       command=self._canvas.yview)
        vs.grid(row=1, column=2, sticky=NS)

        self._canvas.configure(
            scrollregion=(0, 0, 2000, 1000),
            xscrollcommand=hs.set,
            yscrollcommand=vs.set)

        self.rowconfigure(1, weight=1)
        self.columnconfigure(1, weight=1)

        return

#---------------------------------------------------------------------#

def main():
    top = Tk()

    c = ScrolledCanvas(top)
    c.grid(row=1, column=2, sticky=NSEW)

    top.rowconfigure(1, weight=1)
    top.columnconfigure(2, weight=1)

    top.mainloop()

main()

#--#
