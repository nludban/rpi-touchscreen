#!/usr/bin/env python3
#
# 21_tabs_grid.py
#

from tkinter import *

top = Tk()
top.configure(background='red')

# XXX Make the upper frame grow.
top.rowconfigure(1, weight=1)
top.columnconfigure(1, weight=1)

df = Frame(top, background='green')
df.grid(row=1, column=1, sticky=NSEW)

t1 = Frame(df, background='gray')
Label(t1, text='#1 NSEW').grid(row=1, column=1, sticky=NSEW, padx=10, pady=10)

t2 = Frame(df, background='orange')
Label(t2, text='#2 NS').grid(row=1, column=1, sticky=NS)

t3 = Frame(df, background='yellow')
Label(t3, text='#3 EW').grid(row=1, column=1, sticky=EW)

t4 = Frame(df, background='blue')
Label(t4, text='#4 -').grid(row=1, column=1)

for tf in ( df, t1, t2, t3, t4 ):
    tf.rowconfigure(1, weight=1)
    tf.columnconfigure(1, weight=1)

def show_tab(t):
    t1.grid_forget()
    t2.grid_forget()
    t3.grid_forget()
    t4.grid_forget()
    t.grid(row=1, column=1, sticky=NSEW)# , anchor=CENTER)
show_tab(t1)

bf = Frame(top, background='blue')
bf.grid(row=2, column=1, sticky=EW, padx=5, pady=10)

for column in ( 11, 22, 33, 42 ):
    bf.columnconfigure(column, weight=1)

Button(bf, text='Door #1', command=lambda : show_tab(t1),
       height=3).grid(
    row=100, column=11, pady=3, sticky=NSEW)

Button(bf, text='Door #2', command=lambda : show_tab(t2)).grid(
    row=100, column=22, sticky=NS)

Button(bf, text='Door #3', command=lambda : show_tab(t3)).grid(
    row=100, column=33, sticky=EW, padx=3)

Button(bf, text='Door #4', command=lambda : show_tab(t4)).grid(
    row=100, column=42, padx=3)

top.mainloop()

#--#
