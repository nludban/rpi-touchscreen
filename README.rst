#######################################################################
rpi-touchscreen/README.rst
#######################################################################

This was a presentation given at the central ohio python meetup on
2019-01-28.  Clone the repo and open presentation.hml in a browser
to view (make the font bigger and resize the window so the blue headers
make minimum height "slides").

Note each "[number]" in the headers will have a corresponding
screenshot.png and .py files.

***********************************************************************
Abstract
***********************************************************************

The official raspberry pi touchscreen had just been released and did
not have any easy python application development support.  The aging
tkinter may not be the obvious choice for modern touchscreen support,
but it's actually a good fit for quickly adding a customized UI to
a small screen dedicated to a single app.

Target audience is beginners, main purpose of the presentation is to
to get some trivial starter windows working, while emphasizing common
tk/tkinter design patterns that will make it easier to continue
learning using other sources.
