#!/usr/bin/env python3

from tkinter import *

class DiagramFrame(Frame):

    def __init__(self, master, **kwargs):
        Frame.__init__(self, master, **kwargs)

        self._canvas = Canvas(self, width=720, height=480)
        self._canvas.grid(row=1, column=1, sticky=NSEW)

        hs = Scrollbar(self, orient=HORIZONTAL,
                       command=self._canvas.xview)
        hs.grid(row=2, column=1, sticky=EW)

        vs = Scrollbar(self, orient=VERTICAL,
                       command=self._canvas.yview)
        vs.grid(row=1, column=2, sticky=NS)

        self.rowconfigure(1, weight=1)
        self.columnconfigure(1, weight=1)

        return

#---------------------------------------------------------------------#

class Tool:
    W = 50
    H = 40

    def draw(self, canvas, x, y):
        z = 2
        self._status_rectangle = canvas.create_rectangle(
            x+z, y+z, x+self.W-z, y+self.H-z,
            outline='#000000', width=2*z, fill='',
            tag=self.TAG)
        canvas.create_rectangle(x, y, x+self.W, y+self.H,
                                fill='#c0c0c0', #outline='',
                                tag=self.TAG)
        return

    def _show_status(self, canvas, color):
        canvas.itemconfigure(self._status_rectangle, outline=color)
        canvas.tag_raise(self._status_rectangle)
        return

    def _hide_status(self, canvas):
        canvas.tag_lower(self._status_rectangle)
        return


class MoveTool(Tool):
    TAG = 'MoveTool'
    def draw(self, canvas, x, y):
        Tool.draw(self, canvas, x, y)
        canvas.create_text(x+5, y+5,
                           text='Move', anchor=NW,
                           tag=self.TAG)
        return

class BoxTool(Tool):
    TAG = 'BoxTool'
    def draw(self, canvas, x, y):
        Tool.draw(self, canvas, x, y)
        canvas.create_text(x+5, y+5,
                           text='Box', anchor=NW,
                           tag=self.TAG)
        return

class TextTool(Tool):
    TAG = 'TextTool'
    def draw(self, canvas, x, y):
        Tool.draw(self, canvas, x, y)
        canvas.create_text(x+5, y+5,
                           text='Text', anchor=NW,
                           tag=self.TAG)
        return

class PortTool(Tool):
    TAG = 'PortTool'
    def draw(self, canvas, x, y):
        Tool.draw(self, canvas, x, y)
        canvas.create_text(x+5, y+5,
                           text='Port', anchor=NW,
                           tag=self.TAG)
        return

class LineTool(Tool):
    TAG = 'LineTool'
    def draw(self, canvas, x, y):
        Tool.draw(self, canvas, x, y)
        canvas.create_text(x+5, y+5,
                           text='Line', anchor=NW,
                           tag=self.TAG)
        return

class PanTool(Tool):
    TAG = 'PanTool'
    def draw(self, canvas, x, y):
        Tool.draw(self, canvas, x, y)
        canvas.create_text(x+5, y+5,
                           text='Pan', anchor=NW,
                           tag=self.TAG)
        return

class ToolsFrame(Frame):

    TOOLS = (
        MoveTool,
        BoxTool,
        TextTool,
        PortTool,
        LineTool,
        PanTool,
    )

    def __init__(self, master, **kwargs):
        Frame.__init__(self, master, **kwargs)

        self._tools = { }	# TAG -> Tool

        self._canvas = self._create_tools(self)
        self._canvas.grid()

        self._hover = None	# Mouse pointer last seen here.
        self._maybe = None	# Pressed w/o release.
        self._active = self._tools['MoveTool']
        self._show_active(self._active)

        return

    def _create_tools(self, parent):
        c = Canvas(parent,
                   width=Tool.W,
                   height=(Tool.H * len(self.TOOLS)))

        for k, t in enumerate(self.TOOLS):
            t = t()
            self._tools[t.TAG] = t
            t.draw(c, x=0, y=40*k)

        c.bind('<Leave>', self.on_leave)
        c.bind('<Motion>', self.on_motion)
        c.bind('<ButtonPress-1>', self.on_press)
        c.bind('<ButtonRelease-1>', self.on_release)
        return c

    def _show_active(self, t):
        t._show_status(self._canvas, '#222222')

    def _hide_status(self, t):
        t._hide_status(self._canvas)

    def _show_hover(self, t):
        t._show_status(self._canvas, '#888888')

    def _show_maybe(self, t):
        t._show_status(self._canvas, '#22cc22')

    def _get_hover(self, event):
        #x, y = event.x, event.y
        #tid, = self._canvas.find_closest(x, y, halo=None)
        #tags = self._canvas.gettags(tid)
        tid = self._canvas.find_withtag(CURRENT)
        tags = self._canvas.gettags(tid)
        for tag in tags:
            if tag in self._tools:
                return self._tools[tag]
        return None

    def on_leave(self, event):
        print('leave')#, x, y, tags)
        # Stop showing the hover-above tool, except when it's
        # actively being pressed.
        if self._hover is not None and self._maybe is None:
            print('bye', self._hover)
            self._hide_status(self._hover)
            self._hover = None
        return

    def on_motion(self, event):
        tool = self._get_hover(event)
        #print('motion', tool)

        # tool is None (mouse is outside toolbox)
        # ==> Hide all
        if tool is None:
            if self._hover is not None:
                self._hide_status(self._hover)
                self._hover = None
            
        # maybe is not None
        # ==> maybe is active
        #     ==> Ignore everything (until button release)
        #     otherwise
        #     ==> Highlight when over pressed tool
        if self._maybe is not None:
            if self._maybe is self._active:
                pass
            else:
                if tool is not self._hover:
                    if self._hover is not None:
                        self._hide_status(self._hover)
                        self._hover = None

        # otherwise
        # ==> Hover and highlight tool (unless already active)
        else:
            if tool is not self._hover:
                if self._hover is not None:
                    self._hide_status(self._hover)
                    self._hover = None
                if tool is not self._active:
                    self._show_hover(tool)
                    self._hover = tool

        return

    def on_press(self, event):
        tool = self._get_hover(event)
        print('press', tool)

        # Exit hover mode.
        if self._hover is not None:
            self._hide_status(self._hover)
        self._hover = tool

        self._show_maybe(tool)
        self._maybe = tool

        return

    def on_release(self, event):
        tool = self._get_hover(event)
        print('release', tool)

        self.on_motion(event)

        if self._hover is self._maybe:
            self._hide_status(self._active)
            self._active = self._maybe
            self._hover = None
            self._maybe = None
        else:
            # Ignore click+drag.
            self._hide_status(self._maybe)
            self._maybe = None
            self._hover = None

        self._show_active(self._active)
        return

#---------------------------------------------------------------------#

def main():
    top = Tk()

    df = DiagramFrame(top)
    df.grid(row=1, column=2, sticky=NSEW)

    tf = ToolsFrame(top)
    tf.grid(row=1, column=1, sticky=N)

    top.rowconfigure(1, weight=1)
    top.columnconfigure(2, weight=1)

    top.mainloop()

main()

#--#
