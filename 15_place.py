#!/usr/bin/env python3

from tkinter import *

top = Tk()
top.configure(background='yellow')

Label(top, text='one').place(x=50, y=50)
Label(top, text='2').place(x=50, y=100,
                           width=45, height=75,
                           anchor=NE)
Label(top, text='Three').place(x=100, y=100)

top.mainloop()

#--#
