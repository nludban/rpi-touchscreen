#!/usr/bin/env python3
#
# 11_hello.py -- Dehydrated hello world.
#

from tkinter import *

top = Tk()

Label(top, text='Hello, world!', font=('Courier', 42)).pack()

top.mainloop()

#--#
